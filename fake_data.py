
import data_utilis as du
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy import stats



def check_sequence(seq):

    for n ,x in enumerate(seq):
        if ( x >seq[ n -1] ) &(n!=0):

            seq[n] = seq[ n -1]

    if np.sum(seq <0 )!=0:
        seq[seq <0] =  0

    return seq
def _fft_power(x, N=None):
    if N is not None:
        pow = (np.sum(2 * x[1:] ** 2) + x[0] ** 2)/N
    else:
        pow = (np.sum(2*x[1:]**2)+x[0]**2)

    return pow


def generate_fake_blm(n_parts ,n_turns ,left_particles=0, phase_shift=0,
                      f=100 ,revt = 2.5e-6 ,A = 1e-5 ,plot=False ,warnings = True):
    ''' A is not related to the amplitude of the tune ripple but it's in particles unit (applide directly to the particles count)
    the final time binning is given in turn number just for homogeneity'''
    if type(n_parts)!= int:
        n_parts = int(n_parts)
    if type(n_turns)!=int:
        n_turns = int(n_turns)

    ### WARNING FOR THE SATURATION
    if ((n_parts -left_particles ) /n_turns <= 2* np.pi * f * A * revt) & (warnings):
        print('#' * 50)
        A_MAX = (n_parts - left_particles) / (n_turns * 2 * np.pi * f * revt)
        print(f'SATURATION WARNING: lower the amplitude <= {A_MAX} ')
        print('#' * 50)

    #t = np.linspace(0, n_turns - 1, n_turns, dtype=int) * revt
    t = np.arange(0, n_turns, dtype=int) * revt

    t_final = np.linspace(0, n_turns - 1, n_turns, dtype=int)  ## * revt
    if phase_shift is not None:
        phase_shift = phase_shift%(2*np.pi)
        sine = A * np.sin(2 * np.pi * f * t + phase_shift)
    else:
        sine = A * np.sin(2 * np.pi * f * t)
    part_at_turn = check_sequence(np.linspace(n_parts, left_particles, n_turns, dtype=int) + np.ceil(sine))
    if plot:
        plt.plot(part_at_turn)
        plt.xlabel('Turn number')
        plt.ylabel('Number of circulating particles')
    return part_at_turn.astype(int)






def add_poisson(binning):
    '''Adding some random errors'''

    lam = np.mean(binning[1])
    #### should I use a gaussian centered on 0 with std=sqrt(lam) ???
    # errors = np.random.uniform(-np.sqrt(lam)/2,np.sqrt(lam)/2,size=len(binning[1]))
    bins = np.random.poisson(lam, size=len(binning[1]))
    errors = bins - lam
    binN = binning[1] + np.ceil(errors)
    mask_neg = binN < 0
    binN[mask_neg] = 0
    return binning[0], binN.astype(int)


def bin_fake_data(part_at_turn, turn_binning=250, revt=2.5e-6, plot=False, add_errors=True,
                  debug=False, normalize_counts=False, normalize_parts=False, normalize_turns=False):
    '''Binning the data, adding Poisosn's errors with few normalization options (default None)'''
    # MODIFIED VERSION FOR DIFFERENT NORMALIZATION
    #assert not ((normalize_counts | normalize_parts) & normalize_turns), 'only one has to be given'
    bincount = np.array([part_at_turn[n] - part_at_turn[n + 1] for n in range(len(part_at_turn) - 1)])
    if np.sum(bincount < 0) != 0:
        print('SOMETHING IS STRANGE ABOUT PARTICLES COUNT SIGNAL')

    binning = []
    time_step = []
    n_binnings = round(len(bincount) / turn_binning)

    for x in range(n_binnings):
        if x == n_binnings - 1:  # last one will contain a bit more or less than exactly turn_binning counting
            binning.append(np.sum(bincount[x * turn_binning:]))
        else:
            binning.append(np.sum(bincount[x * turn_binning: x * turn_binning + turn_binning]))

        time_step.append(x * turn_binning * revt)

    binning = np.array(binning)
    if debug:
        print('#' * 30)
        print(f'DEBUGGING THE BINNING')
        print(f'extracted particles = {np.sum(bincount)}, extracted from binned = {np.sum(binning)}')
    if add_errors:
        _, binning = add_poisson((time_step, binning))

    if normalize_counts:  # normalization by the area
        #### ANOTHER NORMALIZATION ACTUALLY
        binning = binning / (np.sum(binning) * time_step[1])
    if normalize_parts:
        binning = binning / (np.sum(binning))
    if normalize_turns:
        binning = binning / (len(bincount)+1)


    if plot:
        plt.bar(time_step, binning, width=time_step[1], edgecolor='black')
    return time_step, binning

def stats_parts(n_parts, n_turns, As=(50, 100, 150, 200, 250), turn_binning=300, plot_curves=False,
                plot_curves_stats=False, plot_lin=True, return_complete= False, **norm):
    '''Runs multiple fake data generation iterating on chosen parameters, for multiple amplitude strengths
    Study the errors between the configurations
    TODO: plot transfer function for A with  1. mean-var with different simulations (turns, parts)
        '''

    if ((type(n_parts) == list)|(type(n_parts) is np.ndarray)) & ((type(n_turns) == list)|(type(n_turns) is np.ndarray)):
        iteration_type = 'Particles&Turns'
        assert len(n_parts) == len(n_turns), 'is its a double iteration, they must be same length'
    elif (type(n_parts) is list)|(type(n_parts) is np.ndarray):
        n_turns = [n_turns]*len(n_parts)
        iteration_type = 'Particles'
    elif (type(n_turns) is list)|(type(n_turns) is np.ndarray):
        iteration_type = 'Turns'
        n_parts = [n_parts]*len(n_turns)
    else:
        iteration_type = 'Single'
        n_parts = [n_parts]
        n_turns = [n_turns]
    print(f'DEBUG {iteration_type}')
    #################################### CREATING THE VARIOUS FAKE DATA


    if iteration_type == 'Single':
        print('SINGLE SIMULATION')
        # no iterations on particles or numbers
        peaks100 = []
        for A in As:
            out = generate_fake_blm(n_parts, n_turns, A=A, plot=False, warnings=True)
            out_bins = bin_fake_data(out, turn_binning=turn_binning, **norm)
            fft = du.Ft(out_bins[1], out_bins[0][1], plot=False)
            peaks100.append(fft[1][np.argmin(np.abs(100 - fft[0]))])

        x = As
        y = peaks100
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
        print(f'Slope = {slope}, intercept = {intercept}, R value = {r_value}')
        if plot_lin:
            plt.figure(figsize=(16, 10))
            plt.scatter(As, peaks100)
            plt.ylabel('Amplitude of the peaks')
            plt.xlabel('Amplitude of the oscillation')
            plt.plot(As, np.array(As) * slope + intercept, '--')
        return As, peaks100

    else: ## iterating over particles/turns/both
        sims = []
        for A in As:
            peaks100 = []
            for parts, turns in zip(n_parts,n_turns):
                out = generate_fake_blm(parts, turns, A=A, plot=False, warnings=True)
                out_bins = bin_fake_data(out, turn_binning=turn_binning, **norm)
                fft = du.Ft(out_bins[1], out_bins[0][1], plot=False)
                peaks100.append(fft[1][np.argmin(np.abs(100 - fft[0]))])

            sims.append(dict(blm=out, bins=out_bins, fft=fft, peaks100=np.array(peaks100)))




    ################ ONLY FOR DIFFERENT TURNS AND PARTICLES

    ###### Plot the curves for every amplitude and various parameters (with also a prediction from the linearity of the amplitude)
    proportions = np.array(As) / As[0]
    ### plot to check
    if plot_curves:
        plt.figure(figsize=(16, 10))
    for n, sim in enumerate(sims):
        #### Plot the peaks for each simulation and the prediction given by the proportion to the smaller one
        if (iteration_type == 'Particles') & (plot_curves):
            line, = plt.plot(n_parts, sim['peaks100'], label=f'A = {As[n]}')
            if n != 0:
                plt.plot(n_parts, sims[0]['peaks100'] * proportions[n], '--', c=line.get_color())
        elif (iteration_type == 'Turns') & (plot_curves):
            line, = plt.plot(n_turns, sim['peaks100'], label=f'A = {As[n]}')
            if n != 0:
                plt.plot(n_turns, sims[0]['peaks100'] * proportions[n], '--', c=line.get_color())
        elif (iteration_type == 'Particles&Turns') & (plot_curves):
            line, = plt.plot(sim['peaks100'], label=f'A = {As[n]}')
            if n != 0:
                plt.plot(sims[0]['peaks100'] * proportions[n], '--', c=line.get_color())

    if plot_curves:
        plt.legend()
        plt.ylabel('Amplitude of the FFT peak')
        plt.title(f'Different amplitudes and {iteration_type}')
        if iteration_type == 'Particles&Turns':
            plt.xlabel(f'Different simulations Parts&Turns ')
        else:
            plt.xlabel(f'Number of {iteration_type} ')
    ##################

    ########## Calculate and plot the squared error of the predictions
    errs = []

    for n, sim in enumerate(sims):
        ###squared error on the curve compare to the mean value of the curves
        err = np.sqrt(
            np.mean((np.array(sim['peaks100']) - sims[0]['peaks100'] * proportions[n]) ** 2) / np.mean(sim['peaks100'])**2)
        errs.append(err)

    if plot_curves_stats:
        plt.figure(figsize=(16, 10))
        plt.title('Mean quadratic errors over mean for the previous curves against trivial prediction')
        plt.scatter(As[1:], errs[1:]) # the first will always have error equal to zero

    ######### TRYING TO SEE THE LINEARITY
    mean_peaks = []
    std_peaks = []
    for n, sim in enumerate(sims):
        mean_peak = np.mean(sim['peaks100'])
        std_peak = np.std(sim['peaks100'])  # its not always meaninful
        std_peaks.append(std_peak)
        mean_peaks.append(mean_peak)

    if plot_lin:
        plt.figure(figsize=(16, 10))
        plt.errorbar(As, mean_peaks, yerr=std_peaks, fmt='o', color='blue', ecolor='red', capsize=5)
        plt.xlabel('Amplitudes of the oscillations')
        plt.ylabel('Mean of the peaks ')
        plt.title(f'linearity of the overall curves for different {iteration_type}')
    if return_complete:
        return sims
    else:
        return As, mean_peaks, std_peaks

def evaluate_fft_var(n_parts, n_turns, A=100, reps=100, binnings=None, norm_std=False,
                     plot=True, figsize=(16, 10), **norms):
    '''Function to evaluate the variance as a function of the binning for a give fake simulation
    Only good for single configurations (parts,turns)
            '''
    if binnings is None:
        binnings = [100, 150, 200, 250, 300, 350, 400, 500, 600, 800, 1000]

    print('#' * 50)
    print(f'For every binning value in {binnings} the the height of the peak will be evalued for {reps} times')
    print('#' * 50)

    std_binning = []
    means_binning = []
    for binning in tqdm(binnings):
        peaks100 = []
        for rep in range(reps):
            out = generate_fake_blm(n_parts, n_turns, A=A, warnings=True)
            out_bins = bin_fake_data(out, turn_binning=binning, add_errors=True, **norms)
            fft = du.Ft(out_bins[1], out_bins[0][1], plot=False)
            peaks100.append(fft[1][np.argmin(np.abs(100 - fft[0]))])  ## Could be interpolated
        means_binning.append(np.mean(peaks100))
        if norm_std:
            std_binning.append(np.std(peaks100) / np.mean(peaks100))
        else:
            std_binning.append(np.std(peaks100))

    if plot:

        plt.figure(figsize=figsize)
        plt.errorbar(binnings, means_binning, yerr=std_binning, fmt='o', color='blue', ecolor='red', capsize=5)
        plt.xlabel('Turn binning')
        plt.ylabel('mean of the peak amp')
        plt.title(f'FFT errors estimation for : n_turns {n_turns}, n_parts = {n_parts} ')
    return binnings, std_binning, means_binning





def evaluate_fft_var2(n_parts, n_turns, A=100, reps=20, binnings=None, plot_means=True,
                     plot=True, figsize=(16, 10), add_errors = True, return_all = False, **norms):
    '''Function to evaluate the variance as a function of the binning for a give fake simulation
    #TODO:
            '''
    if binnings is None:
        binnings = [100, 150, 200, 250, 300, 350, 400, 500, 600, 800, 1000]
    if (type(n_parts) is list)&(type(n_turns) is list):
        assert len(n_parts) == len(n_turns), 'Different lengths of n_parts and n_turns!'
        iteration_type = 'Parts&Turns'
    elif (type(n_parts) is list) & (type(n_turns) is not list):
        n_turns = [n_turns] * len(n_parts)
        iteration_type = 'Parts'

    elif (type(n_parts) is not list) & (type(n_turns) is list):
        n_parts = [n_parts] * len(n_turns)
        iteration_type = 'Turns'
    else:
        iteration_type = 'None'
        n_parts = [n_parts]
        n_turns = [n_turns]

    sims = []
    for parts,turns in zip(n_parts,n_turns):
        std_binning = []
        means_binning = []
        for binning in tqdm(binnings):
            peaks100 = []
            for rep in range(reps):
                out = generate_fake_blm(parts, turns, A=A, warnings=True)
                out_bins = bin_fake_data(out, turn_binning=binning, add_errors=add_errors, **norms)
                fft = du.Ft(out_bins[1], out_bins[0][1], plot=False)
                peaks100.append(fft[1][np.argmin(np.abs(100 - fft[0]))])  ## Could be interpolated
            means_binning.append(np.mean(peaks100))
            std_binning.append(np.std(peaks100))
        sims.append(dict(means = means_binning, stds = std_binning, binnings = binnings,parts_turns = (int(parts),int(turns))))



    print('#' * 50)
    print(f'For every binning value in {binnings} the the height of the peak will be evalued for {reps} times')
    print('#' * 50)


    if plot:
        plt.figure(figsize=figsize)
        for sim in sims:
            means_binning = sim['means']
            std_binning = sim['stds']
            plt.errorbar(binnings, means_binning, yerr=std_binning, fmt='o', ecolor='red', capsize=5, label = f'parts/turns = {sim["parts_turns"]}')
        plt.xlabel('Turn binning')
        plt.ylabel('mean of the peak amp')
        plt.legend()
        plt.title(f'FFT errors estimation for : n_turns {n_turns}, n_parts = {n_parts} ')

    if plot_means & plot & (iteration_type != 'None'):
        ### Amplitude averaged over diff binnings for every configuration
        #### PLOT INDEPENDENT OF THE BINNINGS
        plt.figure(figsize=figsize)
        means = []
        stds = []
        for sim in sims:
            means.append(np.mean(sim['means']))
            if ('normalize_counts' in norms.keys()):
                ## Changed for using the errobars with the variance due to the binning
                stds.append(np.mean(sim['stds']))
            else:
                stds.append(np.std(sim['means']))
        if iteration_type == 'Parts':
            x = n_parts
        elif iteration_type == 'Turns':
            x = n_turns
        else: #### SHOULD DO SOMETHING ELSE
            x = n_turns
        plt.errorbar(x, means, yerr=stds, fmt='o', ecolor='red', capsize=5, label = f'parts/turns = {sim["parts_turns"]}')
    if return_all :
        return dict(binnings_intervals = binnings, all_sims = sims)
    else:
        return binnings, std_binning, means_binning


def peak_comparison_statistics(n_parts, n_turns, As=(50, 100, 150, 200, 250), turn_binning=300,window=None,
                               randomize_configurations=True, plot_lin=True, return_complete=False, reps=None,normalize_counts=False,
                               normalize_N=False, compare_mode=None, normalize_turns = False,normalize_particles=False, normalize_parts=False, normalize_turns_aft=False):
    ''' Simulate multiple extractions with different amplitude in the superimposed oscillation, for every amplitude is possible to give different values of particles and turns.
    Calculates finally the peaks at the dominating frequency, and relate them.
    Errobars are calculated and plotted from the repetition of the same anaòlysis multiple times (getting the variance of the peaks ---> if reps is a number => 1


        '''
    assert compare_mode in ['binnings','power',None], 'wrong key'

    if ((type(n_parts) == list) | (type(n_parts) is np.ndarray)) & (
            (type(n_turns) == list) | (type(n_turns) is np.ndarray)):
        iteration_type = 'Particles&Turns'
    elif (type(n_parts) is list) | (type(n_parts) is np.ndarray):
        n_turns = [n_turns] * len(n_parts)
        iteration_type = 'Particles'
    elif (type(n_turns) is list) | (type(n_turns) is np.ndarray):
        iteration_type = 'Turns'
        n_parts = [n_parts] * len(n_turns)
    else:
        iteration_type = 'Single'
        n_parts = [n_parts] * len(As)
        n_turns = [n_turns] * len(As)
    print(f'DEBUG {iteration_type}')
    assert len(n_parts) == len(n_turns), 'is its a double iteration, they must be same length'
    assert len(n_parts) == len(
        As), 'The params for the simulations must either be unique values or arrays of the same length as As'
    #################################### CREATING THE VARIOUS FAKE DATA

    if randomize_configurations:
        n_parts = np.random.choice(n_parts, size=len(n_parts), replace=False)
        n_turns = np.random.choice(n_turns, size=len(n_turns), replace=False)
        As = np.random.choice(As, size=len(As), replace=False)

    peaks100 = []
    peaks_std = []  # COnstaind the peak sdtt var if the param reps is set to a number >1
    for A, parts, turns in zip(As, n_parts, n_turns):
        ### FOR EVERY  extraction CONFIGURATION
        if ((reps is not None) & (reps != 1))|(compare_mode == 'binnings'):
            peaks_instances = []
            if compare_mode=='binnings': #### COMPAIRNG MEANS ACROSS BINNINGS
                print('binnnigs-change')
                for bins in [300,400,500,600,700,800]:
                    out = generate_fake_blm(parts, turns, A=A, plot=False, warnings=True)
                    out_bins = bin_fake_data(out, turn_binning=bins,normalize_parts=normalize_parts,
                                             normalize_counts=normalize_counts, normalize_turns=normalize_turns)
                    fft = du.Ft(out_bins[1], out_bins[0][1], plot=False, normalize_N=normalize_N,
                                normalize_particles=normalize_particles, window = window)
                    if normalize_turns_aft:
                        peaks_instances.append(np.interp(100, fft[0], fft[1])/turns)
                    else:
                        peaks_instances.append(np.interp(100, fft[0], fft[1]))
                peaks100.append(np.mean(peaks_instances))
                peaks_std.append(np.std(peaks_instances))
            else:
                for n in range(reps):
                    out = generate_fake_blm(parts, turns, A=A, plot=False, warnings=True)
                    out_bins = bin_fake_data(out, turn_binning=turn_binning, normalize_parts=normalize_parts,
                                             normalize_counts=normalize_counts, normalize_turns=normalize_turns)
                    fft = du.Ft(out_bins[1], out_bins[0][1], plot=False, normalize_N=normalize_N,window = window,
                                normalize_particles=normalize_particles)
                    if compare_mode == 'power':
                        peaks_instances.append( np.interp(100, fft[0], fft[1])/_fft_power(fft[1]) )
                    elif normalize_turns_aft:
                        peaks_instances.append(np.interp(100,fft[0],fft[1])/turns)
                    else:
                        peaks_instances.append(np.interp(100,fft[0],fft[1]))
                peaks100.append(np.mean(peaks_instances))
                peaks_std.append(np.std(peaks_instances))
        else:
            ### is no reps specified
            pass


    ########## Calculate and plot the squared error of the predictions


    ######### TRYING TO SEE THE LINEARITY

    if plot_lin:
        plt.figure(figsize=(16, 10))
        plt.errorbar(As, peaks100, yerr=peaks_std, fmt='o', color='blue', ecolor='red', capsize=5)
        plt.xlabel('Amplitudes of the oscillations')
        plt.ylabel('Mean of the peaks ')
        plt.title(f'linearity of the overall curves for different {iteration_type}')
    if return_complete:
        # not implemented the differences
        return dict(A = As,part_turns = (n_parts,n_turns), mean_peaks =   peaks100, std_peaks = peaks_std)
    else:
        return dict(A = As,part_turns = (n_parts,n_turns), mean_peaks =   peaks100, std_peaks = peaks_std)


def centered_sine(f=20, A=10, samplef_to_f_ratio=10, n_osc_sig=2, dc_part=0,
                  return_peak=False, shift=0, plot=False, return_sine = False,add_errors = True,
                  norm_samples=False, other_peak_frquencies = None,just_signal=True):
    ''' create a center sinusoidal signal'''
    tp = 1 / f
    t_sample = tp / samplef_to_f_ratio
    t = np.arange(0, n_osc_sig * samplef_to_f_ratio) * t_sample
    sine = A * np.sin(2 * np.pi * f * t + shift) + dc_part
    if add_errors:
        sine = sine+np.random.poisson(dc_part,size=len(sine)) -dc_part
    if just_signal:
        return t,sine
    #print(f'length of the signal is {len(sine)} and formula {round(n_osc_sig * samplef_to_f_ratio)}')
    if plot:
        plt.plot(t, sine)
    ### FFT
    sinefft = du.Ft(sine, t_sample, window=None, plot=False)
    sq_sig = np.sum(sine ** 2)  # signal's power
    sq_fft = (np.sum(2 * sinefft[1][1:] ** 2) + sinefft[1][0] ** 2) / len(sine)  # FFt's power
    if other_peak_frquencies is not None:
        other_peaks = np.interp(other_peak_frquencies, sinefft[0], sinefft[1])
    else:
        other_peaks = None
    if return_sine:
        return sine
    if return_peak:
        if norm_samples:
            peak = np.interp(f, sinefft[0], sinefft[1]) / len(sine)
            if other_peaks is not None:
                other_peaks = other_peaks/len(sine)
        else:
            peak = np.interp(f, sinefft[0], sinefft[1])
        return (sq_sig - sq_fft) * 100 / sq_sig, peak, other_peaks
    else:
        return (sq_sig - sq_fft) * 100 / sq_sig,  other_peaks


def data_centered(n_parts,n_turns=None, turn_binning = None, shift=0, n_osc_sig=2,just_data = False, revT=2.5e-6, binsXosc=3, f=100, plot=False, return_all=False,window = None,
                  normalize_N=False, normalize_turns=False, normalize_particles=False, normalize_parts = False,print_info = True, add_errors = False):
    ''' Create centered dataset and analize it

    - normalize_N: dived fft by N samples
    - normalize_turns: divide bin by N turns
    - divide fft by N particles extracted
    '''
    if type(n_parts) != int:
        n_parts = int(n_parts)

    T = 1 / f
    turnsXosc = round(T / revT)  # number of turns for a complete oscillation

    if n_turns is not None:
        ### Overrides the other parameters for the duration
        n_osc_sig = round(n_turns /turnsXosc)
        n_turns = n_osc_sig * turnsXosc
    else:
        n_turns = n_osc_sig * turnsXosc
    if turn_binning is not None:
        ### Overrides the other parameters for the bionning
        binsXosc = round(turnsXosc/turn_binning)
        turn_binning = binsXosc*turnsXosc
    else:
        turn_binning = round(turnsXosc / binsXosc)  # number of turns for every bin

    blm = generate_fake_blm(n_parts, n_turns, A=50, phase_shift=shift, plot=False)
    ################ WHERE THENORMALIZATIONS ARE APPLIED
    binning = bin_fake_data(blm, turn_binning=turn_binning, add_errors=add_errors,
                                      normalize_counts=False, plot=False,
                            normalize_turns=normalize_turns,normalize_parts=normalize_parts)
    if just_data:
        return binning

    fft_res = du.Ft(binning[1], binning[0][1], plot=False, window=window, normalize_N=normalize_N,
                  normalize_particles=normalize_particles)
    #################
    if print_info:
        print(f'Turns = {n_turns}, binning = {turn_binning} len of signal = {len(binning[1])}')
    ### TO NORMALIZE IN THE SAME WAY
    binning_std = binning[1]
    if normalize_N:
        binning_std = binning_std/len(binning[1])
    if normalize_particles:
        binning_std = binning_std / len(binning[1])


    sq_sum_sign = np.sum(binning_std ** 2)
    sq_sum_fft = (np.sum(2 * fft_res[1][1:] ** 2) + fft_res[1][0] ** 2) / len(binning[1])
    perc_err = (sq_sum_sign - sq_sum_fft) * 100 / sq_sum_sign

    if plot:
        ## plot binning
        pass

    peak = np.interp(f, fft_res[0], fft_res[1])
    if return_all:
        return dict(err_pw = perc_err,peak = peak, pw_sig = sq_sum_sign, pw_fft = sq_sum_fft)
    else:
        return perc_err