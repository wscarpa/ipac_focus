
import os
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
import inspect
from scipy.optimize import curve_fit
from scipy.signal import chirp, get_window
from scipy.fft import fft, fftfreq, fftshift
from scipy.signal import find_peaks


from RFKO_Xsuite import quad_ripples
from RFKO_Xsuite import utility

def params_todict1(filepath,relative_path=False):
    '''Simple read a params file and convert it into a dictionary,
    and return every value as a string to KEEP IN MIND '''

    if relative_path:
        path = './'
    else:
        path = ''
    with open(path+f'{filepath}/params.txt') as file:
        dir_name_ = file.readline().strip()
        dictionary = dict()
        for line in file:
            (key, value) = line.strip().split('=')
            dictionary[key.strip()] = value.strip()
        return dictionary

def dict_from_params(dir = None,subdirs=None,directdir=None,relative_path=False):
    ''' import the parameters of one or many simulations as a dictionary
    '''
    assert (dir is not None)|(directdir is not None), 'no filepath specified'
    # single simulation, giving directly the filename
    if directdir is not None:
        # useful to give the entire path
        return params_todict1(directdir,relative_path=relative_path)
    # more simulations in a directory
    sims_params = dict()
    if subdirs is None: # Do all the subdirs
        for subdir in os.listdir(dir):
            #print(subdir)
            par= params_todict1(f'{dir}/{subdir}',relative_path=relative_path)
            sims_params[subdir] = par
    if subdirs is not None: # single subdir
        if not type(subdirs) is list:
            dictionary = params_todict1(f'{dir}/{subdirs}',relative_path=relative_path)
            return dictionary
        else:
            for subdir in subdirs:
                par= params_todict1(f'{dir}/{subdir}',relative_path=relative_path)
                sims_params[subdir]=par

    return sims_params


def part_to_dict1(filename,relative_path=False):
    if relative_path:
        path='./'
    else:
        path=''
    ## assuming relative paths actually
    dir = path+filename
    at_turn = np.load(f'{dir}/part_at_turn.npy')
    id = np.load(f'{dir}/part_id.npy')
    x0 = np.load(f'{dir}/part0_x.npy')
    px0 = np.load(f'{dir}/part0_px.npy')
    id0 = np.load(f'{dir}/part0_id.npy')
    out = dict(x0 = x0, px0 = px0,id0=id0,at_turn=at_turn,id=id)
    return out



def part_from_sim(dir=None,subdirs=None,directdir = None,relative_path=False):
    outdict = dict()
    assert (directdir is None)!=(dir is None), 'give one of the argument dir or directdir'
    ## Override
    if directdir is not None:
        outdict[directdir] = part_to_dict1(f'{directdir}',relative_path=False)
        return outdict
    if subdirs is None:
        for sim in os.listdir(dir):
            outdict[sim] = part_to_dict1(f'{dir}/{sim}',relative_path=relative_path)
        return outdict
    elif type(subdirs) is list:
        for sim in subdirs:
            outdict[sim] = part_to_dict1(f'{dir}/{sim}',relative_path=relative_path)
    elif type(subdirs) is str:
        try :
            outdict[subdirs] = part_to_dict1(f'{dir}/{subdirs}', relative_path=relative_path)
        except:
            print('No such directory')

        return outdict





def bloss(part,n_turns=None,params=None,labels=None,plot=False,units ='time',figsize=(16,8)):
    ''' extracted the particles countes per turn, also plotting it.
    The result doesn't take into account the number of turns.
    If after a certain turn no particles are extracted, that will be consideredas the last turn number if'''

    assert (n_turns is not None)|(params is not None), 'to avoid bugs the last turn information is needed'
    if 'at_turn' in list(part.keys()):  # ONLY ONE SIMULATION

        at_turn = part['at_turn']
        if n_turns is not None:
            at_turn = np.append(at_turn,n_turns)
        else:
            at_turn = np.append(at_turn,int(params['n_turns']))
        extraction_count = np.bincount(at_turn)[:-1]
        bloss = np.sum(np.bincount(at_turn)) - np.cumsum(extraction_count)
        if plot :
            plt.figure(figsize=figsize)
            if units == 'time': ##### To test
                plt.plot(np.arange(len(bloss))*float(params['revT']), bloss)
            else:
                plt.plot(bloss)
        return bloss
    else:  #  MORE SIMULATIONS AT ONCE

        sims = list(part.keys())
        blosses = dict()
        for sim in sims:
            at_turn = part[sim]['at_turn']
            if n_turns is not None:
                #assumes the n_turns is the same for all the sims
                at_turn = np.append(at_turn, int(n_turns))
            else:
                #this can work with different turn numbers
                at_turn = np.append(at_turn, int(params[sim]['n_turns']))
            extraction_count = np.bincount(at_turn)[:-1]
            blosses[sim] = np.sum(np.bincount(at_turn)) - np.cumsum(extraction_count)
        if plot:
            plt.figure(figsize=figsize)
            for n,bloss in enumerate(blosses.values()):
                if units == 'time':
                    plt.plot(np.arange(len(bloss))*float(params[sims[n]]['revT']), bloss, label=labels[n])
                else:
                    plt.plot(bloss,label=labels[n])
            plt.title('Circulating particles count')
            plt.xlabel('Turn number')
            plt.ylabel('particles count')
            plt.legend()
        return blosses

def Ft(signal, time_int, figsize=(10, 8),plot=True,peaks=3,peaks_finding=False,ret_freqs=False,fig_ax=None,verbose=False,
       starting_bin=0,bar_plot=True,window=None,normalize=False, normalize_N =False, normalize_particles = False):
    ''' Fourier transform of a signal, taking only positive frequencies'''

    if window is not None:
        ## Suppose it's a valid name for a window type
        window_names = ['boxcar', 'triang', 'blackman', 'hamming', 'hann', 'bartlett',
                        'flattop', 'parzen', 'bohman', 'blackmanharris', 'nuttall', 'barthann']
        assert window in window_names, (f'The window type doesn t correspong to a valid window, '
                                        f'for instance {window_names}')
        Window = get_window(window,len(signal))
        signal = signal * Window

    yf = fft(signal)
    xf = fftfreq(len(signal), time_int)

    # Using from zero on and only absolute vals
    xf__ = xf[np.argsort(xf)]
    yf__= np.abs(yf[np.argsort(xf)])
    zero_index = len(xf) // 2
    xf = xf__[zero_index:]
    yf = yf__[zero_index:]



    if plot:
        if fig_ax is not None:
            ax = fig_ax
        else:
            fig, ax = plt.subplots(figsize=figsize)
        if bar_plot:
            # barplot
            ax.bar(xf[starting_bin:],yf[starting_bin:],width=xf[1],edgecolor='black')
            ax.set_xlabel("freq [Hz]")
            ax.set_ylabel("Amplitude [arb.]")
        else:
            # lineplot of the fft
            ax.plot(xf[starting_bin:], yf[starting_bin:])
            ax.xaxis.set_tick_params(rotation=50)
            ax.set_xlabel("freq [Hz]")
            ax.set_ylabel("Amplitude [arb.]")

    ###################### peaks finding
    peakind = []
    cnt = 0
    while (len(peakind) == 0)&(peaks_finding):
        # find peaks using a threshold in height
        height = 0.001
        if cnt > 10 :
            peakind, prop = find_peaks(np.abs(yf), height=0)
            if verbose:
                print('--> height set to zero')
            break

        peakind, prop = find_peaks(np.abs(yf), height=height)
        if len(peakind) == 0: # if no peak is found it lowers the height threshold
            height = height - height * 80 / 100
            cnt+=1
    ###################### peaks finding   END

    ###################### NORMALIZATIONS
    if normalize:
        yf = yf ** 2 * time_int / (len(signal) * xf[1])
    if normalize_N:
        yf = yf/np.sqrt(len(signal))
    if normalize_particles:
        yf = yf/np.sum(signal)
    ##################################

    if peaks_finding:
        fsort = np.argsort(prop['peak_heights'])
    if verbose & peaks_finding:
        print(f'--> the {peaks} heighest peaks are in {xf[peakind[fsort[:-1-peaks:-1]]]}')
        print(f'--> their heights {np.abs(yf[peakind[fsort[:-1-peaks:-1]]])}')
    if plot & peaks_finding: # ADDS THE PEAKS HIGHLIGHTING TO THE PLOT
        plt.scatter(xf[peakind[fsort[:-1-peaks:-1]]], np.abs(yf[peakind[fsort[:-1-peaks:-1]]]), c='r')
    if ret_freqs & peaks_finding:
        return xf, yf,xf[peakind[fsort[:-1-peaks:-1]]]
    return xf, yf


def plot_fft_specifics(x, y, f=100, return_fig=False, fig=None, figsize=(16, 8), alpha=0.5, ranges=(0.9, 1.1),
                       **kwargs):
    # TODO : add colors management for multiple plots

    if 'label' in kwargs:
        label = kwargs['label']
    else:
        label = None
    if 'color' in kwargs:
        color = kwargs['color']
    plot_args = {key: val for key, val in kwargs.items() if key in inspect.getfullargspec(plt.bar).args}

    mask = (x > ranges[0] * f) & (x < ranges[1] * f)

    if fig is None:

        plot = plt.bar(x[mask], y[mask], width=x[1], edgecolor='black', label=label, alpha=alpha, **plot_args)

    else:
        fig.plot(x[mask], y[mask], width=x[1], **plot_args)
    if 'label' in locals():
        plt.legend()

    lims = np.array(plt.xlim()) * np.array([1.1, 0.9])
    if 'color' in locals():
        plt.axhline(y[np.argmin(np.abs(f - x))], linestyle='--', color=color)
    else:
        plt.axhline(y[np.argmin(np.abs(f - x))], linestyle='--', color=plot[0].get_facecolor(), alpha=1)
    # if label is not given?
    if 'label' in locals():
        plt.text(lims[1], y[np.argmin(np.abs(f - x))], label, va='bottom')


def duty_single(filepath, timescale, relative_path=False, time_unit='turns', tbinning=10):
    ''' Time unit is not safe to change!'''
    assert time_unit in ['turn', 'turns', 'sec', 'seconds', 'second'], 'Unsupported time unit'

    params = params_todict1(filepath, relative_path=relative_path)
    # TO DO MANIPULATION DEPENDING ON THE TIME UNIT
    if time_unit in ['turn', 'turns']:
        assert type(timescale) == int, 'Unsupported time'
    else:  # time in seconds
        if tbinning == 10:
            tbinning *= params['revT']
    assert timescale < tbinning

    # modified for the notebook
    out = binning_data(filepath, turn_binning=tbinning, duty_factor=False, normalize_counts=False)
    part_at_turn = np.load(f'{filepath}/part_at_turn.npy')
    bincount = np.bincount(part_at_turn)[:-1]
    n_internal_bins = math.ceil(tbinning / timescale)
    tot_int_bins = []
    tot_int_t = []
    duty_factor = []
    for n, bin in enumerate(out[1]):
        # time_limits in turn units
        # maxtime = out[0][n+1]
        mintime = out[0][n] / float(params['revT'])

        ######## calculating the internal bins
        # FIXME : Cannot use it with the units = seconds option
        internal_bins = [np.sum(bincount[int(mintime + timescale * x):int(mintime + timescale * (x + 1))]) for x in
                         range(n_internal_bins)]
        t = [mintime + timescale * x for x in range(len(internal_bins))]
        bin_duty = np.mean(internal_bins) ** 2 / np.mean(np.array(internal_bins) ** 2)  # duty factor of the parent-bin
        tot_int_bins += internal_bins
        tot_int_t += t
        duty_factor.append(bin_duty)

    plt.figure(figsize=(17, 8))
    plt.bar(out[0], out[1], width=out[0][1], alpha=0.5, align='edge')
    plt.bar(np.array(tot_int_t) * float(params['revT']), tot_int_bins, width=timescale * float(params['revT']),
            edgecolor='black', alpha=0.5, align='edge')
    plt.title('debug plot')

    plt.figure(figsize=(15, 8))
    plt.scatter(np.array(out[0]) / float(params['revT']), duty_factor)
    plt.title(f'Duty factor $T_m =$ {timescale} and $T_c =$ {tbinning}')
    return tot_int_t, tot_int_bins, duty_factor


def duty_mult(folder,binnings=None,plot=False,relative_path=False):
    ''' wrong concept of duty factor'''
    # TODO : fix the function or delete it
    # Assume single folder of one simulation
    if binnings is None:
        params = dict_from_params(direct_dir=folder,relative_path=relative_path)
        n_turns = float(params['n_turns'])
        binnings = np.arange(n_turns/1000,n_turns/10,100,dtype='int')
        #print(binnings)

    dutyF_arr = []
    for bin_size in binnings:
        out = quad_ripples.binning_data(folder,turn_binning=bin_size)
        dtF = out[2]
        #print(dtF)
        dutyF_arr.append(dtF)
    #print(len(dutyF_arr))
    if plot:
        plt.scatter(binnings,dutyF_arr,s=3)
        plt.axhline(1)
        plt.xlabel('Binning [turns]')
        plt.ylabel('Duty factor')
    return binnings, dutyF_arr


def binning_data1(folder_name,turn_binning=5,duty_factor=True,
                 normalize_counts=True,duty_factor_compare_time=None,**kwargs):
    ''' Binning and manipulation of the spill FROM DATA
    RETURN: timestep : istant of time at the beginning of the bin
            binning is the count of extracted particles in the time (timestep[binning]--timestetep[binning+1
        '''
    part_at_turn = np.load(f'{folder_name}/part_at_turn.npy')

    bincount = np.bincount(part_at_turn)[:-1]
    ## Getting revolution period, not explicitly saved
    # real time simulated
    time = utility.read_from_txt(f'{folder_name}/params.txt','time ')
    n_turns = utility.read_from_txt(f'{folder_name}/params.txt','n_turns ',type_='int')
    revt = time/n_turns

    binning = []
    time_step = []

    ## The last turn will in general consider a different number of turns
    # another idea : I could jus elimintae it
    decimal = len(bincount) / turn_binning- len(bincount) // turn_binning
    if decimal>0.5:
        n_binnings = int(len(bincount) // turn_binning) + 1
    else:
        n_binnings = int(len(bincount) // turn_binning) - 1

    for x in range(n_binnings ): # plus or minus one should depend on the remainder
        binning.append(np.sum(bincount[x * turn_binning : x * turn_binning + turn_binning]))
        time_step.append(x * turn_binning*revt)
    binning = np.array(binning)
    if normalize_counts:
        binning = binning/(np.sum(binning)*time_step[1])

    if duty_factor:
        mean_sq = np.mean(binning)**2
        sq_mean = np.mean( np.array(binning)**2)
        duty = mean_sq / sq_mean

        return time_step, binning, duty
    else:
        return time_step, binning


def binning_data(dir=None, subdirs=None, directdir=None, relative_path=False, turn_binning=250, duty_factor=True,
                 normalize_counts=True, plot=True, labels=None, starting_bin=0,err_bar=False, **kwargs):
    '''Binning data from the simulations, can do a single simulation or multiple'''
    #TODO: 1. figsize chek and adjustment 2. doing both err_bar and superimposed plots
    if relative_path:
        path = './'
    else:
        path = ''
    assert (dir is None) != (directdir is None), 'Give either  dir or a directdir value'

    if (dir is not None) & (type(subdirs) is not str):
        outs = dict()
        if subdirs is None:
            # take all subdirectories
            subdirs = os.listdir(path + dir)
        for sim in subdirs:
            outs[sim] = binning_data1(f'{path}{dir}/{sim}', turn_binning=turn_binning, duty_factor=duty_factor,
                                      normalize_counts=normalize_counts)
        if plot:
            if labels is None:
                labels = get_labels(dict_from_params(dir=dir, subdirs=subdirs, directdir=directdir,
                                                     relative_path=relative_path), subdirs)
            if not err_bar:
                plt.figure(figsize=(20, 12))
            # FIXME : Errorbar doesn't work well with multiple plots cause I can't control the colors


            for n, sim in enumerate(outs):
                if err_bar:
                    assert normalize_counts is False, 'err_bar require normalize_counts to be false'
                    plt.figure(figsize=(20,5))
                    yerr = np.sqrt(outs[sim][1][starting_bin:])
                    plt.bar(outs[sim][0][starting_bin:], outs[sim][1][starting_bin:], width=outs[sim][0][1], alpha=0.5,
                            label=labels[n], yerr=yerr)
                    plt.title(labels[n])
                else:
                    plt.bar(outs[sim][0][starting_bin:], outs[sim][1][starting_bin:], width=outs[sim][0][1], alpha=0.5,
                            label=labels[n], edgecolor='black')
            if not err_bar:
                plt.legend()
                plt.title(f'Binning plot ({turn_binning} turns per bin)')
        return outs

    elif (dir is not None) & (type(subdirs) is str):
        out = binning_data1(f'{path}{dir}/{subdirs}', turn_binning=turn_binning, duty_factor=duty_factor,
                            normalize_counts=normalize_counts)

    else:  # direct dir only option
        out = binning_data1(directdir, turn_binning=turn_binning, duty_factor=duty_factor,
                            normalize_counts=normalize_counts)

    if plot:
        plt.figure(figsize=(16, 10))
        if err_bar:
            err_bar = np.sqrt(out[1][starting_bin:])
            plt.bar(out[0][starting_bin:], out[1][starting_bin:], yerr=err_bar, width=out[0][1])
        else:
            plt.bar(out[0][starting_bin:], out[1][starting_bin:],yerr=err_bar, width=out[0][1], edgecolor='black')
        plt.title(f'Binning plot ({turn_binning} turns per bin)')

    return out


def folder_routine(dir=None,subdirs=None,directdir=None,skip_words=None,f_ripple=100,figsize=(20,10),
                   plot_blm=True,plot_fft=True,plot_fit=False,plot_ripple_zoom=True,plot_binning=False,
                   plot_errbar=True,plot_distr=False,no_plot=False,**kwargs):

    if no_plot: # Terrible better to add it to every condition check for the plots
        plot_blm = True
        plot_fft = True
        plot_fit = True
        plot_ripple_zoom = True
        plot_binning = True
        plot_errbar = True
        plot_distr = True

    params = dict_from_params(dir=dir,subdirs=subdirs,directdir=directdir)
    parts = part_from_sim(dir=dir, subdirs=subdirs, directdir=directdir)
    sim_names = list(parts.keys())
    sim_labels = get_labels(params,sim_names,skip_words=skip_words)

    # print info
    print_differences(params)
    if 'starting_bin' in kwargs:
        starting_bin = kwargs['starting_bin']
    else:
        starting_bin = 2
    ## additional params -->  I could need to exclude some specific parameters
    fft_params = {key:val for key,val in kwargs.items() if key in inspect.getfullargspec(Ft).args}
    bin_params = {key: val for key, val in kwargs.items() if key in inspect.getfullargspec(binning_data).args}
    ###################################


    ##### Getting and reordering most datas
    blosses = bloss(parts,labels=sim_labels,plot=plot_blm,figsize=figsize,params=params)
    ## Checking the error bar
    if plot_errbar:
        __ = binning_data(dir=dir, subdirs=subdirs, directdir=directdir, err_bar=True,normalize_counts=False,**bin_params)
    bin_datas = binning_data(dir=dir, subdirs=subdirs, directdir=directdir,plot=plot_binning, **bin_params)



    fft_datas = dict()

    for sim in sim_names:
        fft = Ft(bin_datas[sim][1],bin_datas[sim][0][1],plot=False,**fft_params)
        fft_datas[sim]=fft

    if plot_fft:
        plt.figure(figsize=figsize)
        for label,fft in zip(sim_labels,fft_datas.values()):
            plt.bar(fft[0][starting_bin:],fft[1][starting_bin:],width=fft[0][1],edgecolor='black',alpha=0.5,label=label)

        plt.legend()
        plt.title('FFT of the spill')

    #### ZOOM on the ripple frequency
    if plot_ripple_zoom:
        plt.figure(figsize=(figsize[0]*0.75,figsize[1]))
        for label,fft in zip(sim_labels,fft_datas.values()):
            plot_fft_specifics(fft[0],fft[1],f=f_ripple,label=label)

    ##### initial distribution of extracted_particles
    if plot_distr:
        for param,part in zip(params.values(),parts.values()):
            extracted_distr2(param,part,densities=True,compare_extr=True)
    #### Eliott's fit
    if plot_fit:
        for param,bloss_ in zip(params.values(),blosses.values()):
            fit_bct1(param,bloss_)

    return dict(bin_dict=bin_datas,params_dict=params,parts_dict=parts,labels=sim_labels,
                fft_res=fft_datas,blosses=blosses)





def get_labels(sims_params, sim_keys, skip_words=None):
    ''' Extract labels to name the simulations in plots'''
    labels = []
    if skip_words is None:
        skip_words = ['time_taken_simulation','kick_angle']
    elif type(skip_words) is list:
        skip_words.append('time_taken_simulation')
        skip_words.append('kick_angle') # reduntand with gain and not beutiful to see as a label
    else:
        tmp = [skip_words]
        skip_words = [skip_words, 'time_taken_simulation']

    diff_params = []
    for key in sims_params[sim_keys[0]].keys():
        # check every parameter
        if (sims_params[sim_keys[0]][key] != sims_params[sim_keys[1]][key]) & (key not in skip_words):
            # for every parameter check if its different for the first 2 simulations
            diff_params.append(key)

            for val in sims_params.values():
                labels.append(f'{key} = {val[key]}')
    # more than one parameter would be detected by the procedure
    if len(diff_params) > 1:
        labels=[]
        for val in sims_params.values():
            label = []
            for key in diff_params:
                if key != diff_params[0]:
                    label.append(', ')
                label.append(f'{key} = {val[key]}')
            # print(label)
            labels.append(''.join(label))
            #### Check if the labels could become too long
            ## TODO : ADD OPTION TO ADD SOME WORDS AND RERUN THE FUNCTION WITH THEM AS ADDITIONAL SKIP_WORDS
            if (val==list(sims_params.values())[0])&(len(''.join(label))>50):
                print(f'The label with the parameters given would be {"".join(label)}')
                inp = input('press yes to continue with this or no to interrupt')
                if (inp[0]=='y')|(inp[0]=='Y'):
                    pass
                else :
                    return None
        ##########
        ###

    return labels


def distr_density2(x, y, x1=None, y1=None, figsize=(10, 10), c=None, cmap='viridis', s=4):
    # Create figure and axes
    fig = plt.figure(figsize=figsize)
    ax_scatter = plt.subplot2grid((4, 4), (1, 0), colspan=3, rowspan=3)
    ax_histx = plt.subplot2grid((4, 4), (0, 0), colspan=3)
    ax_histy = plt.subplot2grid((4, 4), (1, 3), rowspan=3)  # , sharey=ax_scatter

    # Scatter plot
    plot = ax_scatter.scatter(x, y, alpha=0.5, c=c, cmap=cmap, s=s, label='extracted')

    # X-axis line plot based on histogram data
    hist_x, bins_x = np.histogram(x, bins=30)
    ax_histx.plot((bins_x[:-1] + bins_x[1:]) / 2, hist_x, color='gray', label='extracted')
    ax_histx.set_xticklabels([])
    # Y-axis line plot based on histogram data
    hist_y, bins_y = np.histogram(y, bins=30)
    ax_histy.plot(hist_y, (bins_y[:-1] + bins_y[1:]) / 2, color='gray')
    ax_histy.set_yticklabels([])

    if (x1 is not None) & (y1 is not None):
        ax_scatter.scatter(x1, y1, alpha=0.4, c='red', s=s, marker='+', label='not extracted')

        hist_x1, bins_x1 = np.histogram(x1, bins=40)
        ax_histx.plot((bins_x1[:-1] + bins_x1[1:]) / 2, hist_x1, color='red', label='not_extracted')
        hist_y1, bins_y1 = np.histogram(y1, bins=40)
        ax_histy.plot(hist_y1, (bins_y1[:-1] + bins_y1[1:]) / 2, color='red')

        ax_histx.legend()
        ax_scatter.legend()

    return plot


def extracted_distr2(params, part, densities=False, compare_extr=True, **kwargs):
    ''' Plot of the extracted particles distribution at the initial time
    TODO : Comparing with the non extracted  somehow'''

    ############# Managing of the parameters
    params_figure = {key: val for key, val in kwargs.items() if key in inspect.getfullargspec(plt.figure).args}
    params_scatter = {key: val for key, val in kwargs.items() if key in inspect.getfullargspec(plt.scatter).args}

    # default parameters
    def_val = dict(cmap='viridis', s=4, alpha=0.5)
    for par in def_val.keys():
        if par not in params_scatter:
            params_scatter[par] = def_val[par]

    params_density = {key: val for key, val in params_scatter.items() if
                      key in inspect.getfullargspec(distr_density2).args}
    ############################

    extr_mask = part['at_turn'] < float(params['n_turns'])
    colors = part['at_turn'][extr_mask]
    idx = part['id'][extr_mask]
    map1 = pd.Series(dict(zip(idx, colors)))
    idx0 = [x for x in part['id0'] if x in idx]
    extr0 = [x in idx0 for x in part['id0']]

    #### Plot the non extracted instead
    idx = part['id'][np.logical_not(extr_mask)]
    idx0_ = [x for x in part['id0'] if x not in idx]

    if densities is False:
        fig = plt.figure(**params_figure)
        plot = plt.scatter(part['x0'][extr0], part['px0'][extr0], c=map1.loc[idx0], **params_scatter)
        plt.title('initial distribution over the extraction time')
        cbar = plt.colorbar(plot)
        cbar.set_label('Extraction turn')

    elif compare_extr:

        plot = distr_density2(part['x0'][extr0], part['px0'][extr0], x1=part['x0'][np.logical_not(extr_mask)],
                              y1=part['px0'][np.logical_not(extr_mask)], c=map1.loc[idx0], **params_density)  # to add s
        # FIXME : The colorbar is a bit problematic in the visualization
        # cbar = plt.colorbar(plot)
        # cbar.set_label('Extraction turn') 

        # plot = distr_density(part['x0'][np.logical_not(extr_mask)], part['px0'][np.logical_not(extr_mask)], c='r', cmap='viridis')
    else:
        plot = distr_density2(part['x0'][extr0], part['px0'][extr0], c=map1.loc[idx0], **params_density)  # to add s


def print_differences(sims_params):
    sim_keys = list(sims_params.keys())
    print('-'*50)
    diffkey = []
    for key in sims_params[sim_keys[0]].keys() :
        diffs = [sims_params[sim_keys[0]][key]!= sims_params[sim_keys[n]][key] for n in range(1,len(sim_keys))]
        
        if (np.sum(diffs)>=1)&(key not in ['n_part','n_turns','time','time_taken_simulation','extr_part']):
            if (key in ['gain','kick_angle'])&(('gain' in diffkey)|('kick_angle' in diffkey)):
                print(f'SKIPPING {key}')
                continue
            diffkey.append(key)
            for sim,val in sims_params.items():
                print(f'{key} = {val[key]} --->> {sim} ')
    print('-'*50)

def fit_bct1(params, beam_loss, init_val=None):
    # at_turn = part['at_turn']
    # bincount = np.bincount(at_turn)[:-1]
    # beam_loss = int(params['n_part']) - np.cumsum(bincount)
    gain = float(params['gain'])

    # print(r'fitting function is : $f(t) = a e^{-b x \text{gain}**2}$')

    def fitf(x, a, b, c):
        return a * np.exp(-b * x * gain ** c)

    t = (np.arange(0, len(beam_loss))) * float(params['revT'])
    coeff, cov = curve_fit(fitf, t, beam_loss, p0=[beam_loss[0], 1, 2], maxfev=10000)
    print(f'a = {coeff[0]}, b = {coeff[1]}, c = {coeff[2]}')

    pred = fitf(t, coeff[0], coeff[1], coeff[2])

    plt.figure(figsize=(15, 8))
    plt.plot(t, beam_loss, label='simulation data')
    plt.plot(t, pred, label='function fit')
    plt.title(
        'fit of $f(t) = a e^{-b x {gain}^c}$' + f' a = {round(coeff[0], 3)}, b = {round(coeff[1], 3)}, c = {round(coeff[2], 3)}',
        fontsize=20)
    plt.legend()

    return


def transfer_function_complete(out_total, ref_param='gain', f=100, figsize=(16, 8), **kwargs):
    outsf = out_total['fft_res']  # fft analysis results
    ref_params = np.array([ou[ref_param] for ou in out_total['params_dict'].values()])
    plot_args = {key: val for key, val in kwargs.items() if key in inspect.getfullargspec(plt.scatter).args}

    amps = []
    for out in outsf.values():
        mask = np.argmin(np.abs(out[0] - f))
        amps.append(out[1][mask])
    amps = np.array(amps)
    idsort = np.argsort(ref_params)
    plt.figure(figsize=figsize)
    plt.scatter(ref_params[idsort], amps[idsort], **plot_args)
    plt.xlabel(f'{ref_param}')
    plt.ylabel('Amplitude')
    plt.title(f'transfer function for the ripple at {f} Hz')


def fix_params_part(filepath, name='extr_part'):
    with open(f'{filepath}/params.txt') as file:
        out = file.readlines()
    exist = False
    for line in out[1:]:
        print(line.strip().split('=')[0].strip())
        if line.strip().split('=')[0].strip() == name:
            print('found already')
            return None

    if exist is False:
        ### CREATE IT
        ini = out[0].find('extr_part') + 9
        fin = out[0][ini:].find('__')
        extr = int(out[0][ini:ini + fin])
        add_line = f'{name} = {extr} \n'
    out.insert(1, add_line)
    with open(f'{filepath}/params.txt', 'w') as file:
        file.writelines(out)

    return out


def sim_procedure(dir=None, sim=None, directdir=None, relative_path=False, binnings=None,
                  turn_binning=250, cut_range=None, starting_bin_fft=3, starting_bin_spill=3, **kwargs):
    # TODO : figsize management
    assert dir != directdir, 'give only "dir" on "directdir"'
    # optional parameters management
    bin_params = {key: val for key, val in kwargs.items() if
                  (key in inspect.getfullargspec(binning_data).args) & (key not in ['turn_binning'])}
    binnings_params = {key: val for key, val in kwargs.items() if (key in inspect.getfullargspec(binning_data).args) & (
                key not in ['turn_binning', 'normalize_counts', 'err_bar'])}

    ## grabbing parameters and data
    params = dict_from_params(dir=dir, subdirs=sim, directdir=directdir, relative_path=relative_path)
    parts = part_from_sim(dir=dir, subdirs=sim, directdir=directdir, relative_path=relative_path)

    labels = [sim]
    ### BLM ploting

    blosses = bloss(parts, plot=True, labels=labels,n_turns=params['n_turns'])
    if binnings is not None:
        # Trying different binnings
        assert type(binnings) == list, 'binnings must be a list'
        assert len(binnings) > 1, ('if a single value, give directly the parameter turn_binning')
        print('=' * 30)
        print('MULTIPLE BINNING INTERVALS CHOSEN ')
        print('=' * 30)
        for binning in binnings:
            bin_data_ = binning_data(dir=dir, subdirs=sim, directdir=directdir, relative_path=relative_path,
                                     turn_binning=binning, normalize_counts=False, err_bar=True,
                                     starting_bin=starting_bin_spill, **binnings_params)
        plt.show()
        print('=' * 30)
        print('=' * 30 + '\n\n')

    print('=' * 15 + ' CONSIDERED BINNING ' + '=' * 15)
    bin_data = binning_data(dir=dir, subdirs=sim, directdir=directdir, relative_path=relative_path,
                            turn_binning=turn_binning, starting_bin=starting_bin_spill, **bin_params)
    plt.show()
    if cut_range is not None:  # cutting the sequence of data
        print('=' * 15 + 'CUTTING THE SEQUENCE' + '=' * 15)
        # assumes you never give as units for cutting in turns
        if (type(cut_range) == list) | (type(cut_range) == tuple):
            assert len(cut_range) == 2, 'buh'
            bin_data_cut = cut_binning(bin_data, turn_binning, cut_range[0], cut_range[1])
            plt.figure(figsize=(16, 10))
            plt.bar(bin_data_cut[0], bin_data_cut[1], width=bin_data[0][1])
            plt.title('binning with cut')
        elif type(cut_range) == float:
            # considered the starting time
            bin_data_cut = cut_binning(bin_data, turn_binning, cut_range)
            plt.figure(figsize=(16, 10))
            plt.bar(bin_data_cut[0], bin_data_cut[1], width=bin_data[0][1], edgecolor='black')
            plt.xlabel('f [Hz]')
            plt.ylabel('amps [arb. units]')
            plt.title('binning with cut')
        plt.show()

    ## FFT
    print('=' * 30)
    print('FFT ')
    print('=' * 30)
    fft0 = Ft(bin_data[1], bin_data[0][1], plot=False)
    plt.figure(figsize=(16, 10))
    plt.bar(fft0[0][starting_bin_fft:], fft0[1][starting_bin_fft:], width=fft0[0][1], edgecolor='black')
    plt.title('data_bin fft')
    if cut_range is not None:
        plt.figure(figsize=(16, 10))
        fft1 = Ft(bin_data_cut[1], bin_data[0][1], plot=False)
        plt.bar(fft1[0][starting_bin_fft:], fft1[1][starting_bin_fft:], width=fft1[0][1], edgecolor='black')
        plt.xlabel('f [Hz]')
        plt.ylabel('amps [arb. units]')
        plt.title('cut data bin fft')
        plt.show()
    print('=' * 30)
    print('=' * 30)
    return dict(bin_data=np.array(bin_data[:2]),params=params,parts_dict=parts,labels=sim,duty_factor=bin_data[2],
                fft_res=dict(fft0 = fft0, fft_cut = fft1),blosses=blosses)



def cut_binning(binned_data, turn_binning, start_time, end_time=None, params=None, units='seconds'):
    # binned_data[0] = np.array(binned_data[0])
    # binned_data[1] = np.array(binned_data[1])
    assert units in ['seconds', 'turns'], ' units mustbe "seconds" or "turns"'
    time_res = binned_data[0][1]
    if units == 'turns':
        assert params is not None, 'giver dictionary of params (with at least revT)'
        start_time = start_time * params['revT']
        end_time = end_time * params['revT']
    if end_time is not None:
        mask = (np.array(binned_data[0]) >= start_time) & (np.array(binned_data[0]) <= end_time)
    else:
        mask = (np.array(binned_data[0]) >= start_time)
    return np.array(binned_data[0])[mask], np.array(binned_data[1])[mask]

